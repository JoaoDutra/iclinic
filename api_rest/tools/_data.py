import time
import datetime
import pytz


def _corrige_fuso(datahora: datetime.datetime, fuso_origem: str, fuso_destino: str):
    dt_origem = pytz.timezone(fuso_origem).localize(datahora)
    dt_destino = dt_origem.astimezone(pytz.timezone(fuso_destino))
    return dt_destino


def _entrada(datahora, formato_origem):
    if datahora in ['now', 'agora']:
        datahora = int(time.time())

    # Se timestamp (O formato do dado tem precedencia sobre "FORMATO_ORIGEM")
    if isinstance(datahora, int)  or isinstance(datahora, float)or str(datahora).isdigit() or (formato_origem == "timestamp"):
        datahora = int(datahora)
        return datetime.datetime.fromtimestamp(datahora)
    elif isinstance(datahora, datetime.datetime) or (formato_origem == "datetime"):

        return datahora
    elif isinstance(datahora, str):

        return datetime.datetime.strptime(datahora, formato_origem)
    else:

        raise Exception("Formato de dado incorreto!")


def _saida(datahora: datetime.datetime, formato_destino: str):
    if formato_destino == "timestamp":
        return int(datetime.datetime.timestamp(datahora))
    else:
        return datahora.strftime(formato_destino)


def formata_datahora(datahora, fuso_origem="UTC", fuso_destino="UTC",
                      formato_origem="%d/%m/%Y %H:%M:%S", formato_destino="%d/%m/%Y %H:%M:%S"):
    """
    Trade Date/Time between timezones and formats
    :param datahora: float, int or str (e.g.: 1583691817, "08/03/2020 16:49:55" or "now")
    :param fuso_origem: str. Timezone of datahora (e.g.: "UTC")
    :param fuso_destino: str. Timezone of the return (e.g.: "America/Sao_Paulo)
    :param formato_origem: str. datahora format
    :param formato_destino: str. return format
    :return:
    """
    # Convertemos o dado para um objeto datetime
    dt = _entrada(datahora, formato_origem)
    # Transformamos o objeto naive em um objeto aware com a timezone corrigida
    dt = _corrige_fuso(dt, fuso_origem, fuso_destino)
    # Formatamos a saida
    dt = _saida(dt, formato_destino)
    return dt
