import hashlib
import requests
import json
from shared.custom_exceptions import ServiceNotAvailableError
from redis import StrictRedis


def get(url, headers=None, timeout=None, max_retrys=0):
    request_params = {
        'url': url,
        'headers': headers,
    }

    request_hash = hashlib.md5(json.dumps(request_params).encode('utf-8')).hexdigest()


    while max_retrys >= 0:
        try:
            r = requests.get(url, headers=headers, timeout=timeout)

            assert r.status_code != 408, "Request timed out"

            return r

        except:
            max_retrys -= 1
            continue
    raise ServiceNotAvailableError("Max retrys reached")


def post(url, body, headers=None, timeout=None, max_retrys=0):
    while max_retrys >= 0:
        try:
            r = requests.post(url, data=body, headers=headers, timeout=timeout)

            assert r.status_code != 408, "Request timed out"

            return r

        except:
            max_retrys -= 1
            continue
    raise ServiceNotAvailableError("Max retrys reached")
