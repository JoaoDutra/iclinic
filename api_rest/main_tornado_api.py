from api_rest.utils_tornado import tornado_server

from api_rest.controllers import alive
from api_rest.controllers.prescriptions import prescriptions

if __name__ == "__main__":

    server = tornado_server.TornadoServer(
        {
            "/api/alive": alive.Alive,
            "/prescriptions": prescriptions.Prescriptions,
        }
    )
    server.start()
