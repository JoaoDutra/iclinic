from api_rest.utils_tornado.tornado_handler import TornadoHandler
import json
class Alive(TornadoHandler):

    async def get(self):
        self.set_status(200)
        self.write(json.dumps({"status": "ok", "response": "Alive"}))
        await self.finish()
