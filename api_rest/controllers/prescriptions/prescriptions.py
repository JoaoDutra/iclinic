from api_rest.utils_tornado.tornado_handler import TornadoHandler

from dto.metricsPayload import MetricsPayload
import json

from schema import Schema

from repositories import prescription_repository, clinic_repository, patient_repository, physician_repository, metrics_repository
from models import Physician, Patient, Prescription, Clinic
from api_rest.controllers.prescriptions.prescriptions_doc import *

prescription_schema = Schema({
  "clinic": {
    "id": int
  },
  "physician": {
    "id": int
  },
  "patient": {
    "id": int
  },
  "text": str
})

class Prescriptions(TornadoHandler):
    @TornadoHandler.exception_handler()
    async def post(self):
        """
        ---
        tags:
         - Prescriptions
        summary: Add a new prescription
        operationId: addPrescription
        requestBody:
         description: Prescription object that needs to be added to the system
         content:
           application/json:
             schema:
               $ref: '#/components/schemas/PrescriptionModel'
         required: true
        responses:
            '200':
                description: Success
                content:
                 application/json:
                   schema:
                     $ref: '#/components/schemas/PrescriptionResponseModel'
            '400':
                description: Bad request
                content:
                 application/json:
                   schema:
                     $ref: '#/components/schemas/ExceptionModel'
            '404':
                description: Not found
                content:
                 application/json:
                   schema:
                     $ref: '#/components/schemas/ExceptionModel'
            '500':
                description: Internal error
                content:
                 application/json:
                   schema:
                     $ref: '#/components/schemas/ExceptionModel'
            '503':
                description: Service not available
                content:
                 application/json:
                   schema:
                     $ref: '#/components/schemas/ExceptionModel'


        """

        body = json.loads(self.request.body)
        prescription_schema.validate(body)

        id_physician, id_clinic, id_patient = self.extrac_data_body(body)

        physician = self.get_physician(id_physician)
        clinic = self.get_clinic(id_clinic)
        patient = self.get_patient(id_patient)

        prescription = Prescription(clinic, physician, patient, body['text'])

        connection = self.mysql_connection()
        repo = prescription_repository.PrescriptionRepository(connection)
        prescription_id = repo.save(prescription)

        prescription.id = prescription_id

        try:
            metric_result = self.send_metrics(physician, patient, prescription, clinic)
        except Exception as e:
            connection.rollback()
            raise e
        else:
            connection.commit()

        await self.end_request(200, metric_result)


    def extrac_data_body(self, body):
        id_physician = body['physician']['id']
        id_clinic = body['clinic']['id']
        id_patient = body['patient']['id']
        return id_physician, id_clinic, id_patient

    def get_physician(self, id_physician):
        physician_token = self.config['Physicians']['token']
        physician_request = physician_repository.get_by_id(id_physician, token=physician_token)
        physician = Physician(**physician_request)
        return physician

    def get_clinic(self, id_clinic):
        try:
            clinic_auth_token = self.config['Clinics']['token']
            clinic_request = clinic_repository.get_by_id(id_clinic, token=clinic_auth_token)
            clinic = Clinic(**clinic_request)
            return clinic
        except:
            clinic = Clinic(id=id_clinic)
            return clinic

    def get_patient(self, id_patient):
        patient_token = self.config['Patients']['token']
        patient_request = patient_repository.get_by_id(id_patient, token=patient_token)
        patient = Patient(**patient_request)
        return patient

    def send_metrics(self, physician, patient, prescription, clinic):
        metrics_payload = MetricsPayload(physician, patient, prescription, clinic)
        metric_token = self.config['Metrics']['token']
        metric_result = metrics_repository.send_data(metrics_payload, token=metric_token)
        return metric_result