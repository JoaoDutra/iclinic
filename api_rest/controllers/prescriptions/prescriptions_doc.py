
from tornado_swagger.components import components

@components.schemas.register
class ExceptionModel(object):
    """
    ---
    type: object
    description: Model of exception response
    properties:
        code:
            type: string
            description: Code of exception
        message:
            type: string
            description: Message of exception
    """


@components.schemas.register
class IdObjectModel(object):
    """
    ---
    type: object
    description: Model of object with id used many times
    properties:
        id:
            type: integer
            description: id of the object
            example: 1
    """

@components.schemas.register
class PrescriptionModel(object):
    """
    ---
    type: object
    description: Prescription model representation
    properties:
        clinic:
            $ref: '#/components/schemas/IdObjectModel'
        physician:
            $ref: '#/components/schemas/IdObjectModel'
        patient:
            $ref: '#/components/schemas/IdObjectModel'
        text:
            type: string
            description: Text of the prescription
            example: 'Take 2 tablets of ibuprofen'
    """

@components.schemas.register
class PrescriptionResponseModel(object):
    """
    ---
    type: object
    description: Prescription response model representation
    properties:
        id:
            type: str
            description: id of the prescription
            example: 896496fa-eade-462c-8970-88cea894ec7d
        clinic_id:
            type: integer
            description: id of the clinic
            example: 1
        clinic_name:
            type: string
            description: name of the clinic
            example: 'Clinic 1'
        physician_id:
            type: integer
            description: id of the physician
            example: 1
        physician_name:
            type: string
            description: name of the physician
            example: 'Dr. Smith'
        physician_crm:
            type: string
            description: crm of the physician
            example: '123456'
        patient_id:
            type: integer
            description: id of the patient
            example: 1
        patient_name:
            type: string
            description: name of the patient
            example: 'John Doe'
        patient_email:
            type: string
            description: email of the physician
            example: 'teste@teste.com'
        patient_phone:
            type: string
            description: phone of the patient
            example: '+5511987654321'
        text:
            type: string
            description: text of the prescription
            example: 'Take 2 tablets of ibuprofen'
    """
