#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
# IMPORTS
"""


import sys
import signal
import logging

# Dependencias Tornado
import tornado.web
import tornado.gen
import tornado.ioloop
import tornado.options

import configparser
import os

from shared.databases.mysql_connector import MySqlConnector

from tornado_swagger.setup import setup_swagger
"""
# CONFIG
"""


INTERNAL_CONFIG_TORNADO = {'xheaders': True}


"""
# PAYLOAD
"""
class App(tornado.web.Application):

    def __init__(self, routes, *args, **kwargs):
        setup_swagger(
            routes,
            swagger_url="/docs",
            description="",
            api_version="1.0.0",
            title="API DOCS.",
        )
        super(App, self).__init__(routes, *args, **kwargs)

class TornadoServer(object):

    def __init__(self, endpoint_mapping, **kwargs):

        config = configparser.ConfigParser()

        config.read('./tornado_api.cfg')
        self.config_wrapper(config)

        self.log_level = 'INFO'
        if 'TORNADO' in config:
            config_tornado = config['TORNADO']

            if 'LOG_LEVEL' in config_tornado:
                self.log_level = config_tornado['LOG_LEVEL']

        self.context = {
            "config": config,
            "log_level": self.log_level,
        }

        if 'MYSQL' in config:
            config_mysql = config['MYSQL']

            self.mysql_connector = MySqlConnector(
                config_mysql['host'],
                config_mysql['user'],
                config_mysql['password'],
                config_mysql['db']
            )

            self.context.update({
                "mysql_connection": self.mysql_connector.db_connection
            })


        # Context of all endpoints


        # Endpoint list initialization
        self.endpoints = []

        # Para cada endpoint e sua classe handler
        for endpoint, classe in endpoint_mapping.items():

            # Incluimos na lista o objeto tupla do endpoint, já incluindo o contexto
            self.endpoints.append((r"{}".format(endpoint), classe, self.context))

        # Tornado app start
        self.app_tornado = App(self.endpoints)

        # SIGTERM monitoring to shutdown
        signal.signal(signal.SIGTERM, self.shutdown)

        # SIGINT monitoring to shutdown
        signal.signal(signal.SIGINT, self.shutdown)

    def start(self, aditional_text=''):

        # Mapeamos a aplicação do servidor Tornado para a porta desejada
        if '--port' in sys.argv:
            pos = sys.argv.index('--port')
            port = int(sys.argv[pos+1])
        else:
            raise KeyError("Nao foi possivel identificar a PORTA do servidor!")
        self.app_tornado.listen(port, **INTERNAL_CONFIG_TORNADO)

        # Iniciamos o servidor tornado
        logging.warning(
            "[{}]: Starting server {}!".format(port, aditional_text))

        # Executamos o comando de início do tornado
        tornado.ioloop.IOLoop.instance().start()

    def shutdown(self, sinal, frame):
        u"""
        Função que desliga o servidor tornado em execução
        @return: None
        """

        # Logamos a parada iminente
        logging.warning("Recebido sinal {}! Parando o Servidor!".format(sinal))

        # Executamos o comando de finalizar o tornado
        tornado.ioloop.IOLoop.instance().stop()

        # Lançamos o log final
        logging.warning("Servidor desligado!")

        # Desligamos o processo
        sys.exit(0)

    def config_wrapper(self, config):
        for section in config.sections():
            for option in config[section]:
                value = config[section][option]
                if value.startswith('$'):
                    try:
                        config[section][option] = os.environ[value[1:]]
                    except KeyError as ke:
                        raise Exception("Config option '{}' of section '{}' not found in environment".format(option, section))
