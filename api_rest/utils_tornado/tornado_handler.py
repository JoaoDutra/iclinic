from tornado.web import RequestHandler
import logging
import traceback
from tornado.log import LogFormatter
from datetime import datetime
from functools import wraps
from shared.custom_exceptions import *
import json


class TornadoHandler(RequestHandler):

    """
    Sobrescrita do método initialize do RequestHandler
    """
    def initialize(self, **kwargs):
        # Copiamos completament os kwargs e configurações de setup
        self.kwargs = kwargs

        # Criamos um alias para os kwargs, o "contexto"
        self.context = self.kwargs

        self.config = self.context['config']
         # Definimos a database e collection de log de erros
        try:
            self.error_log_database = self.config["TORNADO"]["error_log_database"]
        except:
            self.error_log_database = 'LOG_ERROS'
        try:
            self.error_log_collection = self.config["TORNADO"]["error_log_collection"]
        except:
            self.error_log_collection = 'LOG_ERROS'

        self.classe = str(self.__class__.__name__)
        self.server_name = self.config["TORNADO"]["server_name"]

        # Criamos um alias para o logger
        # Inicializamos o logger da classe
        tag = "{}".format(self.__class__.__name__)
        self.logger = logging.getLogger(tag)
        self.logger.propagate = False
        channel = logging.StreamHandler()
        channel.setFormatter(LogFormatter(
            fmt='%(color)s[%(levelname)1.1s %(asctime)s]%(end_color)s [' + tag + '] %(message)s'))
        self.logger.handlers = []
        self.logger.addHandler(channel)
        self.logger.setLevel(self.context.get('log_level', 'INFO'))
        self.tags_string = ""

        self.decode_token = {}
        self.formt_token = ""
        self.token = ""

    def set_default_headers(self):
        self.add_header("Access-Control-Allow-Origin", "*")
        self.add_header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
        self.add_header("Content-Type", "application/json")
        self.add_header("Access-Control-Allow-Headers",
                        "x-requested-with,contentType,Content-Type,authorization,Authorization")

    def instancia_logger(self, nivel):

        # Selecionamos o objeto de LOG
        if nivel == "DEBUG":
            objeto_log_local = self.logger.debug
        elif nivel == "INFO":
            objeto_log_local = self.logger.info
        elif nivel == "WARNING":
            objeto_log_local = self.logger.warning
        elif nivel == "ERROR":
            objeto_log_local = self.logger.error
        else:
            raise Exception("Nivel de LOG inexistente! As opcoes sao: DEBUG, INFO, WARNING e ERROR")
        return objeto_log_local

    def log(self, texto, nivel="INFO"):

        try:
            objeto_log_local = self.instancia_logger(nivel)
            # Formatamos e escrevemos a mensagem
            mensagem = '{} : {}'.format(self.tags_string, texto)
            objeto_log_local(mensagem)

        except Exception as exp:
            self.logger.error("Erro no LOG:")
            self.logger.error(exp)
            self.logger.error("Mensagem que gerou erro no LOG:")
            self.logger.error(texto)
            self.logger.error("Traceback do erro no LOG:")
            self.logger.error(traceback.format_exc())

    def log_err(self, texto):
        self.addLogTag("ERRO")
        trace = str(traceback.format_exc())
        # print(trace)
        self.log(trace, nivel='ERROR')
        self.log(texto, nivel='ERROR')
        #
        # dict_dados = {
        #     'headers': self.request.headers if hasattr(self, 'request') else None,
        #     'body': self.request.body if hasattr(self, 'request') else None,
        # }
        #
        # try:
        #     self.conexao_mongo(self.database_log_erros)[self.colecao_log_erros].insert_one({
        #         'tipo_servidor': 'TORNADO',
        #         'nome_servidor': self.nome_servidor,
        #         'classe': self.classe,
        #         'mensagem_erro': str(texto),
        #         'stack_trace': trace.splitlines() if isinstance(trace, str) else None,
        #         # 'data_hora_salvamento_int': agora_int,
        #         # 'data_hora_salvamento_str': agora_str,
        #         'log_tags': self.tags_string,
        #         'python': str(sys.version_info),
        #         'dados': dict_dados,
        #     })
        # except Exception as exp:
        #     self.log("Erro ao salvar log de erros no mongo", nivel='ERROR')

    def addLogTag(self, tag):
        self.tags_string = f"[{str(tag)}]"

    def prepare(self):
        self.timestamp_begin = datetime.now()
        self.log("[BEGIN]")

    def on_finish(self):
        self.timestamp_end = datetime.now()

        duracao = self.timestamp_end - self.timestamp_begin

        self.log(f"[END {duracao.total_seconds()}s]")

    async def end_request(self, status_code, response):
        self.set_status(status_code)
        self.write(json.dumps(response))
        await self.finish()

    async def end_exception_request(self, exception, status_code, response, log_level='ERROR'):
        self.log(exception, nivel=log_level)
        await self.end_request(status_code, response)

    @staticmethod
    def exception_handler(*args, **kwargs):
        def wrap(func):
            @wraps(func)
            async def wrapped_func(self, *args, **kwargs):
                try:
                    return await func(self, *args, **kwargs)
                except MalformedRequest as e:

                    response = {
                        "code": "01",
                        "message": "malformed request"
                    }
                    await TornadoHandler.end_exception_request(self, e, 400, response, log_level='WARNING')

                except PhysicianNotFound as e:
                    response = {
                        "code": "02",
                        "message": "physician not found"
                    }
                    await TornadoHandler.end_exception_request(self, e, 404, response, log_level='WARNING')

                except PatientNotFound as e:
                    response = {
                        "code": "03",
                        "message": "patient not found"
                    }
                    await TornadoHandler.end_exception_request(self, e, 404, response, log_level='WARNING')

                except MetricsServiceNotAvailable as e:
                    response = {
                        "code": "04",
                        "message": "metrics service not available"
                    }
                    await TornadoHandler.end_exception_request(self, e, 503, response, log_level='WARNING')

                except PhysiciansServiceNotAvailable as e:
                    response = {
                        "code": "05",
                        "message": "physicians service not available"
                    }
                    await TornadoHandler.end_exception_request(self, e, 503, response, log_level='WARNING')

                except PatientsServiceNotAvailable as e:
                    response = {
                        "code": "06",
                        "message": "patients service not available"
                    }
                    await TornadoHandler.end_exception_request(self, e, 503, response, log_level='WARNING')

                except Exception as e:

                    response = {
                        "code": "500",
                        "message": "internal server error"
                    }
                    await TornadoHandler.end_exception_request(self, e, 500, response, log_level='WARNING')

            return wrapped_func

        return wrap

    def mysql_connection(self):
        return self.context.get('mysql_connection')