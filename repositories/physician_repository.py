from shared.custom_exceptions import PhysiciansServiceNotAvailable, PhysicianNotFound, ServiceNotAvailableError
from api_rest.tools import _requests as requests_maker
import requests_cache

requests_cache.install_cache('physician_repository_cache', backend='sqlite', expire_after=48*3600)

def get_by_id(physician_id, token=''):
    try:
        headers = {
            'Authorization': token
        }

        url = "https://mock-api-challenge.dev.iclinic.com.br/physicians/{}/".format(physician_id)
        r = requests_maker.get(url, headers=headers, timeout=4, max_retrys=2)

        if r.status_code == 404:
            raise PhysicianNotFound("Physician with id {} not found".format(physician_id))

        assert r.status_code == 200, "Request failed with code {}".format(r.status_code)

        return r.json()
    except ServiceNotAvailableError as snae:
        raise PhysiciansServiceNotAvailable(snae)
