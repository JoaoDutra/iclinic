from shared.custom_exceptions import PatientsServiceNotAvailable, PatientNotFound, ServiceNotAvailableError
from api_rest.tools import _requests as requests_maker
import requests_cache

requests_cache.install_cache('patient_repository_cache', backend='sqlite', expire_after=12*3600)

def get_by_id(patient_id, token=''):
    try:
        headers = {
            'Authorization': token
        }
        url = "https://mock-api-challenge.dev.iclinic.com.br/patients/{}/".format(patient_id)
        r = requests_maker.get(url, headers=headers, timeout=3, max_retrys=2)

        if r.status_code == 404:
            raise PatientNotFound("Patient whith id {} not found".format(patient_id))

        assert r.status_code == 200, "Request failed with code {}".format(r.status_code)

        return r.json()
    except ServiceNotAvailableError as snae:
        raise PatientsServiceNotAvailable(snae)
