from shared.custom_exceptions import ServiceNotAvailableError
from api_rest.tools import _requests as requests_maker
import requests_cache

requests_cache.install_cache('clinic_repository_cache', backend='sqlite', expire_after=72*3600)

def get_by_id(clinic_id, token=''):
    try:
        headers = {
            'Authorization': token
        }
        url = "https://mock-api-challenge.dev.iclinic.com.br/clinics/{}/".format(clinic_id)
        r = requests_maker.get(url, headers=headers, timeout=5, max_retrys=3)

        assert r.status_code == 200, "Request failed with code {}".format(r.status_code)

        return r.json()
    except ServiceNotAvailableError as e:
        raise ServiceNotAvailableError(e)
