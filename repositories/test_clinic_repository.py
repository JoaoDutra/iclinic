import unittest
import responses
import json
from repositories import clinic_repository


class TestClinicRepository(unittest.TestCase):
    model = {
        "id": 1,
        "name": "Lopes"
    }

    not_found_model = {"detail": "Not found."}

    clinic_url = "https://mock-api-challenge.dev.iclinic.com.br/clinics/{}/"

    @responses.activate
    def test_get_clinic_by_existent_id(self):

        clinic_id = 1

        url = self.clinic_url.format(clinic_id)

        responses.add(**{
            'method': responses.GET,
            'url': url,
            'body': json.dumps(self.model),
            'status': 200,
            'content_type': 'application/json',
        })

        clinic = clinic_repository.get_by_id(clinic_id)

        self.assertDictEqual(clinic, self.model)


    @responses.activate
    def test_get_clinic_by_inexistent_id(self):

        clinic_id = 1000

        url = self.clinic_url.format(clinic_id)

        responses.add(**{
            'method': responses.GET,
            'url': url,
            'body': json.dumps(self.not_found_model),
            'status': 404,
            'content_type': 'application/json',
        })
        with self.assertRaises(Exception):
            clinic_repository.get_by_id(clinic_id)
