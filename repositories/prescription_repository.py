from models.prescription import Prescription

class PrescriptionRepository:

    def __init__(self, connection):
        self.connection = connection

    def save(self, prescription: Prescription):
        sql_insert_query = """ INSERT INTO prescriptions (clinic_id, physician_id, patient_id, text) VALUES (%s, %s, %s, %s) """
        values = (prescription.clinic.id, prescription.physician.id, prescription.patient.id, prescription.text)

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(sql_insert_query, values)
        return cursor.lastrowid