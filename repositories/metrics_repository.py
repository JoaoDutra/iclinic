from dto.metricsPayload import MetricsPayload
from api_rest.tools import _requests as requests_maker
from shared.custom_exceptions import MetricsServiceNotAvailable, ServiceNotAvailableError


def send_data(metric_payload: MetricsPayload, token=''):
    try:
        headers = {
            'Authorization': token
        }
        url = "https://mock-api-challenge.dev.iclinic.com.br/metrics/"
        r = requests_maker.post(url, body=metric_payload.get_metrics_payload(), headers=headers, timeout=6, max_retrys=5)

        assert r.status_code == 201, "Request failed with code {}".format(r.status_code)

        return r.json()
    except ServiceNotAvailableError as snae:
        raise MetricsServiceNotAvailable(snae)
