import unittest
import responses
import json
from repositories import patient_repository
from shared.custom_exceptions import PatientNotFound

class TestClinicRepository(unittest.TestCase):
    model = {
        "id": 1,
        "name": "Pedro Henrique Aragão",
        "email": "anacampos@nogueira.net",
        "phone": "(051) 2502-3645"
    }

    not_found_model = {"detail": "Not found."}

    patient_url = "https://mock-api-challenge.dev.iclinic.com.br/patients/{}/"

    @responses.activate
    def test_get_patient_by_existent_id(self):

        patient_id = 1

        url = self.patient_url.format(patient_id)

        responses.add(**{
            'method': responses.GET,
            'url': url,
            'body': json.dumps(self.model),
            'status': 200,
            'content_type': 'application/json',
        })

        patient = patient_repository.get_by_id(patient_id)

        self.assertDictEqual(patient, self.model)


    @responses.activate
    def test_get_patient_by_inexistent_id(self):

        patient_id = 1000

        url = self.patient_url.format(patient_id)

        responses.add(**{
            'method': responses.GET,
            'url': url,
            'body': json.dumps(self.not_found_model),
            'status': 404,
            'content_type': 'application/json',
        })
        with self.assertRaises(PatientNotFound):
            patient_repository.get_by_id(patient_id)
