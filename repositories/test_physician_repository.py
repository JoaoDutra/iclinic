import unittest
import responses
import json
from repositories import physician_repository
from shared.custom_exceptions import PhysicianNotFound

class TestClinicRepository(unittest.TestCase):
    model = {
        "id": 1,
        "name": "Teste",
        "crm": "75423070"
    }

    not_found_model = {"detail": "Not found."}

    physician_url = "https://mock-api-challenge.dev.iclinic.com.br/physicians/{}/"

    @responses.activate
    def test_get_physician_by_existent_id(self):

        physician_id = 1

        url = self.physician_url.format(physician_id)

        responses.add(**{
            'method': responses.GET,
            'url': url,
            'body': json.dumps(self.model),
            'status': 200,
            'content_type': 'application/json',
        })

        physician = physician_repository.get_by_id(physician_id)

        self.assertDictEqual(physician, self.model)


    @responses.activate
    def test_get_physician_by_inexistent_id(self):

        physician_id = 1000

        url = self.physician_url.format(physician_id)

        responses.add(**{
            'method': responses.GET,
            'url': url,
            'body': json.dumps(self.not_found_model),
            'status': 404,
            'content_type': 'application/json',
        })

        with self.assertRaises(PhysicianNotFound):
            physician_repository.get_by_id(physician_id)
