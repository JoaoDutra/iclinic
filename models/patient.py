class Patient:
    def __init__(self, id, name, email, phone, *kwargs):
        self.id = id
        self.name = name
        self.email = email
        self.phone = phone

    def __str__(self):
        return f'Patient: {self.id} {self.name} {self.email} {self.phone}'