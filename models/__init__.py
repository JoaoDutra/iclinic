from .physician import Physician
from .patient import Patient
from .clinic import Clinic
from .prescription import Prescription