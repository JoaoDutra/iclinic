class Physician:
    def __init__(self, id, name, crm, *kwargs):
        self.id = id
        self.name = name
        self.crm = crm

    def __str__(self):
        return f'{self.name} - {self.crm}'