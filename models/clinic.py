class Clinic:
    def __init__(self, id=None, name=None, *kwargs):
        self.id = id
        self.name = name

    def __str__(self):
        return f"{self.id} - {self.name}"