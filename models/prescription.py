from models import Physician, Patient, Clinic

class Prescription:

    def __init__(self, clinic: Clinic, physician: Physician, patient: Patient, text: str, *kwargs):
        self.id = None
        self.clinic = clinic
        self.physician = physician
        self.patient = patient
        self.text = text

    def __str__(self):
        return f"{self.id} {self.clinic} {self.physician} {self.patient} {self.text}"