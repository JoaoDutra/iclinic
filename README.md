## 📝 Table of contents

- [Prerequisites](#developmentrequirements)
- [Run (Docker)](#rundocker)
- [Run (Without Docker)](#runnodocker)
- [Env file](#envfile)
- [API url](#url)
- [Docs](#docs)
- [API routes](#routesapi)
- [Next steps](#comments)



## 📝 Prerequisites <a name="developmentrequirements"></a>

- Python3
- Env file (env.list)
- Docker (Opcional)


## 💭 Run (Docker) <a name="rundocker"></a>

1.Install Docker:
https://docs.docker.com/engine/install/ubuntu/

2.Clone the repository:
```terminal
$ git clone git@gitlab.com:JoaoDutra/iclinic.git
```
3.Go to project folder:
```terminal
$ cd iClinic
```
4.Run the build command:
```terminal
$ docker build -f Dockerfile.dockerfile -t iclinic:latest .
```
5.Run the container:
```terminal
$ docker run --rm --name iclinic -p 9001:9001 --env-file ./env.list iclinic:latest
```

## 💭 Run (Without Docker) <a name="runnodocker"></a>

1.Clone the repository:
```terminal
$ git clone git@gitlab.com:JoaoDutra/iclinic.git
```
2.Go to project folder:
```terminal
$ cd iClinic
```
3.Install the python requirements:
```terminal
$ pip install -r requirements.txt
```
4.Run:
```terminal
$ export PATIENTS_TOKEN='PUT_THE_PATIENTS_API_TOKEN_HERE' && export PYTHONPATH='/YOUR/PATH/TO/PROJECT/ROOT' && export PHYSICIANS_TOKEN='PUT_THE_PHYSICIANS_API_TOKEN_HERE' && export METRICS_TOKEN='PUT_THE_METRICS_API_TOKEN_HERE' && export MYSQL_USER=YOUR_MYSQL_USER_HERE && export MYSQL_URL=YOUR_MYSQL_URL_HERE && export MYSQL_PASSWORD=YOUR_MYSQL_PASSWORD_HERE && export CLINICS_TOKEN='PUT_THE_CLINICS_API_TOKEN_HERE' && cd '/YOUR/PATH/TO/PROJECT/ROOT/api_rest' && /YOUR/PATH/TO/PYTHON/python '/YOUR/PATH/TO/PROJECT/ROOT/api_rest/main_tornado_api.py' --port 9001
```

## 🌎 Env file <a name="envfile"></a>

This file contains the environment variables used by the docker container.
Example:
```env
PATIENTS_TOKEN=Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJzZXJ2aWNlIjoicGF0aWVudHMifQ.Pr6Z58GzNRtjX8Y09hEBzl7dluxsGiaxGlfzdaphzVU
PHYSICIANS_TOKEN=Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJzZXJ2aWNlIjoicGh5c2ljaWFucyJ9.Ei58MtFFGBK4uzpxwnzLxG0Ljdd-NQKVcOXIS4UYJtA
METRICS_TOKEN=Bearer SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c
MYSQL_USER=iclinicuser
MYSQL_URL=iclinic.cwxoox0uls98.us-east-1.rds.amazonaws.com
MYSQL_PASSWORD=iclinicuser
CLINICS_TOKEN=Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJzZXJ2aWNlIjoiY2xpbmljcyJ9.r3w8KS4LfkKqZhOUK8YnIdLhVGJEqnReSClLCMBIJRQ
```

## 🌐 API url <a name="url"></a>

<a href=http://ec2-54-146-141-18.compute-1.amazonaws.com:9001>ec2-54-146-141-18.compute-1.amazonaws.com:9001</a>

## 📜 Docs <a name="docs"></a>

<a href=http://ec2-54-146-141-18.compute-1.amazonaws.com:9001/docs>ec2-54-146-141-18.compute-1.amazonaws.com:9001/docs</a>

## 📲 API routes <a name="routesapi"></a>

| Método  | Rota           |
| ------------- |----------------|
| POST | /prescriptions |
| GET | /api/alive |


---


## 🔰 Next steps <a name="comments"></a>

- Increase tests coverage
- Kubernetes deployment
- Helm chart