class NotFoundError(Exception):
    pass


class ServiceNotAvailableError(Exception):
    pass


class MalformedRequest(Exception):
    pass


class PhysicianNotFound(Exception):
    pass


class PhysiciansServiceNotAvailable(Exception):
    pass


class PatientNotFound(Exception):
    pass


class PatientsServiceNotAvailable(Exception):
    pass


class MetricsServiceNotAvailable(Exception):
    pass