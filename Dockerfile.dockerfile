FROM python:3.6-alpine

COPY . /iclinic

RUN pip install -r /iclinic/requirements.txt

WORKDIR /iclinic/api_rest

EXPOSE 9001

ENTRYPOINT PYTHONPATH=/iclinic python main_tornado_api.py --port 9001