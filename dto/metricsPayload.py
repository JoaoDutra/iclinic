from models import Physician, Patient, Clinic, Prescription

class MetricsPayload():

    def __init__(self, physician: Physician, patient: Patient, prescription: Prescription, clinic: Clinic=None, *kwargs):
        self.clinic_id = clinic.id if clinic else None
        self.clinic_name = clinic.name if clinic else None
        self.physician_id = physician.id
        self.physician_name = physician.name
        self.physician_crm = physician.crm
        self.patient_id = patient.id
        self.patient_name = patient.name
        self.patient_email = patient.email
        self.patient_phone = patient.phone
        self.prescription_id = prescription.id

    def get_metrics_payload(self):
        return vars(self)
