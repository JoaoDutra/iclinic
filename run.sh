#!/bin/sh

set -e

cd ~
echo "Stoping container"
docker stop iclinic || true

echo "$CI_REGISTRY_USER"
echo "$CI_REGISTRY_PASSWORD"
echo "$CI_REGISTRY"

echo "Docker login"
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

echo "Pulling image"
docker pull $CI_REGISTRY_IMAGE:latest
echo "Run"
docker run --rm --name iclinic -p 9001:9001 --env-file ./env.list -d $CI_REGISTRY_IMAGE:latest
